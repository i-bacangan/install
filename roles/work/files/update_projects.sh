#!/bin/bash

for project in /workspace/work/*comm*; do
  cd $project
  docker run --rm -v /workspace/repo/m2:/root/.m2 -v $(pwd):/ws -w /ws maven mvn install || docker run --rm -u root -v /workspace/repo/m2:/home/gradle/.m2 -v $(pwd):/ws -w /ws gradle gradle install || true
done

for project in /workspace/work/*; do
  cd $project
  docker run --rm -v /workspace/repo/m2:/root/.m2 -v $(pwd):/ws -w /ws maven mvn install || docker run --rm -u root -v /workspace/repo/m2:/home/gradle/.m2 -v $(pwd):/ws -w /ws gradle gradle install || true
done